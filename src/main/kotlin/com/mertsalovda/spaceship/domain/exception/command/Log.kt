package com.mertsalovda.spaceship.domain.exception.command

import com.mertsalovda.spaceship.domain.common.ICommand

class Log(
    private val command: ICommand,
    private val exception: Exception
) : ICommand {

    override fun execute() {
        println("${command.javaClass.name}: ${exception.stackTrace}")
    }
}