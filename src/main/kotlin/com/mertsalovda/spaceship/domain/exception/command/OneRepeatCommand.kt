package com.mertsalovda.spaceship.domain.exception.command

import com.mertsalovda.spaceship.domain.common.ICommand

class OneRepeatCommand(
    private val command: ICommand
) : ICommand {

    override fun execute() {
        command.execute()
    }
}