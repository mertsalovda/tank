package com.mertsalovda.spaceship.domain.exception

open class CommandException : Exception()