package com.mertsalovda.spaceship.domain.exception

import com.mertsalovda.spaceship.domain.common.ICommand
import kotlin.reflect.KClass

class ExceptionHandler {

    private val handlers = hashMapOf<Pair<KClass<out ICommand>, KClass<out Exception>>, (ICommand, Exception) -> Unit>()

    fun handle(command: ICommand, exception: Exception) {
        val key = Pair(command.javaClass.kotlin, exception.javaClass.kotlin)
        handlers[key]?.invoke(command, exception)
    }

    fun setup(
        commandType: KClass<out ICommand>,
        exceptionType: KClass<out Exception>,
        action: (ICommand, Exception) -> Unit
    ) {
        val key = Pair(commandType, exceptionType)
        handlers[key] = action
    }
}