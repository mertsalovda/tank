package com.mertsalovda.spaceship.domain.exception

class FuelIsEmptyException : CommandException()