package com.mertsalovda.spaceship.domain.common

import com.mertsalovda.spaceship.domain.common.Vector


interface IMovable {
    var position: Vector
    val velocity: Vector
}