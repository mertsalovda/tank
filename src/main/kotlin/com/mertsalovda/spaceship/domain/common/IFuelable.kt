package com.mertsalovda.spaceship.domain.common

interface IFuelable {
    var fuel: Int
    val burnFuelOnPath: Int
}