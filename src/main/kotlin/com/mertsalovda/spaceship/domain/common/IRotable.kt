package com.mertsalovda.spaceship.domain.common

interface IRotable {

    var direction: Int
    val angularVelocity: Int
    val directionNumber: Int
}