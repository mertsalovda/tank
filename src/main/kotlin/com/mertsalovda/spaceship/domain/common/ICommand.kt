package com.mertsalovda.spaceship.domain.common

interface ICommand {

    fun execute()
}