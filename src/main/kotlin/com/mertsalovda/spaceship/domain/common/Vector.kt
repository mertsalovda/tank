package com.mertsalovda.spaceship.domain.common

data class Vector(
    val x: Int,
    val y: Int
)

operator fun Vector.plus(vector: Vector) = Vector(x + vector.x, y + vector.y)