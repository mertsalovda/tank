package com.mertsalovda.spaceship.domain.command.fuel

import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.common.IFuelable
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.plus
import kotlin.math.pow
import kotlin.math.sqrt

class BurnFuelCommand(
    private val movable: IMovable,
    private val fuelable: IFuelable
) : ICommand {
    override fun execute() {
        val position = movable.position
        val newPosition = position + movable.velocity
        val pathX = position.x - newPosition.x
        val pathY = position.y - newPosition.y
        val path = sqrt(pathX.toFloat().pow(2) + pathY.toFloat().pow(2)).toInt()
        val fuel = fuelable.fuel
        val burnFuel = fuelable.burnFuelOnPath * path
        fuelable.fuel = fuel - burnFuel
    }
}