package com.mertsalovda.spaceship.domain.command

import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.IRotable

class ChangeVelocityCommand(
    private val movable: IMovable,
    private val rotable: IRotable
) : ICommand {

    override fun execute() {
    }
}