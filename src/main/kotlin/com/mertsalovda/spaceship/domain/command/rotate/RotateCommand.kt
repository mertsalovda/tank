package com.mertsalovda.spaceship.domain.command.rotate

import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.common.IRotable

class RotateCommand(
    private val rotable: IRotable
) : ICommand {

    override fun execute() {
        rotable.direction = (rotable.direction + rotable.angularVelocity) % rotable.directionNumber
    }
}