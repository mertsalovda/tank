package com.mertsalovda.spaceship.domain.command.move

import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.plus

class MoveCommand(
    private val movable: IMovable
) : ICommand {

    override fun execute() {
        movable.position = movable.position + movable.velocity
    }
}