package com.mertsalovda.spaceship.domain.command

import com.mertsalovda.spaceship.domain.common.ICommand

class MacroCommand(
    private val commands: List<ICommand>
) : ICommand {

    override fun execute() {
        commands.forEach { command ->
            command.execute()
        }
    }
}