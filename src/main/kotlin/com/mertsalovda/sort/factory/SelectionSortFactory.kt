package com.mertsalovda.sort.factory

import com.mertsalovda.sort.SelectionSort
import com.mertsalovda.sort.Sorter

class SelectionSortFactory : SorterFactory {
    override fun createSorter(): Sorter {
        return SelectionSort()
    }
}