package com.mertsalovda.sort.factory

import com.mertsalovda.sort.BubbleSort
import com.mertsalovda.sort.Sorter

class BubbleSortFactory : SorterFactory {
    override fun createSorter(): Sorter {
        return BubbleSort()
    }
}