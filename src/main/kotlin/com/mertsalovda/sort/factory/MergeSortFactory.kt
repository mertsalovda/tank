package com.mertsalovda.sort.factory

import com.mertsalovda.sort.MergeSort
import com.mertsalovda.sort.Sorter

class MergeSortFactory : SorterFactory {
    override fun createSorter(): Sorter {
        return MergeSort()
    }
}