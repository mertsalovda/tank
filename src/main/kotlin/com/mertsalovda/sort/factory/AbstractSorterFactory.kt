package com.mertsalovda.sort.factory

interface AbstractSorterFactory {
    fun createSorter(type: String): SorterFactory?
}