package com.mertsalovda.sort.factory

import com.mertsalovda.sort.Sorter

interface SorterFactory {
    fun createSorter(): Sorter
}