package com.mertsalovda.sort.factory

class AbstractSorterFactoryImpl : AbstractSorterFactory {

    private val factories: MutableMap<String, () -> SorterFactory> = mutableMapOf()

    override fun createSorter(type: String): SorterFactory? {
        return factories[type]?.invoke()
    }

    fun setup(type: String, factory: () -> SorterFactory) {
        factories[type] = factory
    }
}