package com.mertsalovda.sort

import com.mertsalovda.sort.factory.AbstractSorterFactoryImpl
import com.mertsalovda.sort.factory.BubbleSortFactory
import com.mertsalovda.sort.factory.MergeSortFactory
import com.mertsalovda.sort.factory.SelectionSortFactory
import com.mertsalovda.sort.helpers.SortHelper

fun main() {
    SortHelper(
        sortType = "selection",
        nonSortedFilePath = "src/main/resources/non_sorted_array",
        sortedFilePath = "src/main/resources/sorted_array"
    ).sort()
}


fun initAbstractFactory(abstractSorterFactory: AbstractSorterFactoryImpl) {
    abstractSorterFactory.setup("selection") {
        SelectionSortFactory()
    }
    abstractSorterFactory.setup("bubble") {
        BubbleSortFactory()
    }
    abstractSorterFactory.setup("merge") {
        MergeSortFactory()
    }
}
