package com.mertsalovda.sort

class MergeSort : Sorter {
    override fun sort(array: Array<Int>): Array<Int> {
        if (array.size <= 1) {
            return array
        }

        val middle = array.size / 2
        var left = array.copyOfRange(0, middle)
        var right = array.copyOfRange(middle, array.size)

        left = sort(left)
        right = sort(right)


        return merge(left, right)
    }

    private fun merge(left: Array<Int>, right: Array<Int>): Array<Int> {
        val result = IntArray(left.size + right.size)

        var i = 0
        var j = 0
        var k = 0

        while (i < left.size && j < right.size) {
            result[k++] = if (left[i] <= right[j]) left[i++] else right[j++]
        }

        while (i < left.size) {
            result[k++] = left[i++]
        }

        while (j < right.size) {
            result[k++] = right[j++]
        }

        return result.toTypedArray()
    }
}