package com.mertsalovda.sort

class SelectionSort : Sorter {
    override fun sort(array: Array<Int>): Array<Int> {
        for (i in array.indices) {
            var minIndex = i
            for (j in i + 1 until array.size) {
                if (array[j] < array[minIndex]) {
                    minIndex = j
                }
            }
            if (minIndex != i) {
                val temp = array[i]
                array[i] = array[minIndex]
                array[minIndex] = temp
            }
        }
        return array
    }
}