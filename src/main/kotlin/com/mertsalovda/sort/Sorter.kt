package com.mertsalovda.sort

interface Sorter {
    fun sort(array: Array<Int>): Array<Int>
}