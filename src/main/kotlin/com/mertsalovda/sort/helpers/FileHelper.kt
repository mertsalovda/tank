package com.mertsalovda.sort.helpers

import java.io.File

class FileHelper(private val path: String) {

    fun readArray(): Array<Int> {
        val number = mutableListOf<Int>()
        File(path).forEachLine { line ->
            number.addAll(line.split(" ").map { it.toInt() })
        }
        return number.toTypedArray()
    }

    fun writeArrayToFile(sortType: String, array: Array<Int>, path: String) {
        val file = File(path)
        if (file.exists().not()) {
            val fileCreated = file.createNewFile()
            val isWritable = file.setWritable(true)
            println("Файл создан: $fileCreated, в файл можно записывать: $isWritable")
            if (!fileCreated || !isWritable) return
        }
        val stringBuilder = StringBuilder(sortType + "\n")
        array.forEach { number -> stringBuilder.append("$number ") }
        file.writeText(stringBuilder.toString().trim())
    }
}