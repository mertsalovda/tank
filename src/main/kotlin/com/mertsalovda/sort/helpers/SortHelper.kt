package com.mertsalovda.sort.helpers

import com.mertsalovda.sort.factory.AbstractSorterFactoryImpl
import com.mertsalovda.sort.initAbstractFactory


class SortHelper(
    private val sortType: String,
    private val nonSortedFilePath: String,
    private val sortedFilePath: String
) {

    fun sort() {
        val abstractSorterFactory = AbstractSorterFactoryImpl()

        initAbstractFactory(abstractSorterFactory)

        val fileHelper = FileHelper(path = nonSortedFilePath)
        val array = fileHelper.readArray()

        val sorterFactory = abstractSorterFactory.createSorter(sortType)

        val sortedArray = sorterFactory?.createSorter()?.sort(array)

        sortedArray?.let { array -> fileHelper.writeArrayToFile(sortType, array, sortedFilePath) }
    }
}