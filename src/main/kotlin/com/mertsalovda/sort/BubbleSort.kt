package com.mertsalovda.sort

class BubbleSort : Sorter {
    override fun sort(array: Array<Int>): Array<Int> {
        val result = arrayOf(*array)
        val n = result.size
        for (i in 0 until n - 1) {
            for (j in 0 until n - i - 1) {
                if (result[j] > result[j + 1]) {
                    val temp = result[j]
                    result[j] = result[j + 1]
                    result[j + 1] = temp
                }
            }
        }
        return result
    }
}