import com.mertsalovda.spaceship.domain.exception.ExceptionHandler
import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.exception.command.Log
import com.mertsalovda.spaceship.domain.exception.command.OneRepeatCommand
import com.mertsalovda.spaceship.domain.exception.command.TwoRepeatCommand
import java.util.LinkedList
import java.util.Queue

private val commandQueue: Queue<ICommand> = LinkedList()
private val exceptionHandler = ExceptionHandler()

fun main(args: Array<String>) {

    initExceptionHandler(exceptionHandler)

    while (true) {
        val command = commandQueue.poll()
        try {
            command.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(command, exception)
        }
    }
}

fun initExceptionHandler(exceptionHandler: ExceptionHandler) {

    exceptionHandler.setup(
        commandType = ICommand::class,
        exceptionType = Exception::class,
        action = { command, _ ->
            commandQueue.add(TwoRepeatCommand(command))
        }
    )

    exceptionHandler.setup(
        commandType = TwoRepeatCommand::class,
        exceptionType = Exception::class,
        action = { command, _ ->
            commandQueue.add(OneRepeatCommand(command))
        }
    )

    exceptionHandler.setup(
        commandType = OneRepeatCommand::class,
        exceptionType = Exception::class,
        action = { command, exception ->
            commandQueue.add(Log(command, exception))
        }
    )
}