package com.mertsalovda.spaceship.domain.rotate

import com.mertsalovda.spaceship.domain.command.rotate.RotateCommand
import com.mertsalovda.spaceship.domain.common.IRotable
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertFails

internal class RotateCommandTest {

    /**
     * Для объекта, со стартовым значением поворота 0 и угловой скоростью 45, поворот на 45
     */
    @Test
    fun `1) rotate to direction 45`() {
        // Arrange
        val startDirection = 0
        val angularVelocity = 45
        val directionNumber = 360
        val expectedDirection = 45

        val stubRotable = object : IRotable {
            override var direction: Int = startDirection
            override val angularVelocity: Int = angularVelocity
            override val directionNumber: Int = directionNumber
        }

        val rotateCommand = RotateCommand(stubRotable)
        // Act
        rotateCommand.execute()
        val actualDirection = stubRotable.direction

        // Assert
        assertEquals(expectedDirection, actualDirection)
    }

    /**
     * Попытка повернуть объект, у которого невозможно прочитать direction, приводит к ошибке
     */
    @Test
    fun `2) exception if direction fail`() {
        // Arrange
        val angularVelocity = 45
        val directionNumber = 360

        val stubRotable = object : IRotable {
            override var direction: Int
                get() = throw Throwable()
                set(value) {}
            override val angularVelocity: Int = angularVelocity
            override val directionNumber: Int = directionNumber
        }

        val rotateCommand = RotateCommand(stubRotable)

        // Act
        // Assert
        assertFails {
            rotateCommand.execute()
        }
    }

    /**
     * Попытка повернуть объект, у которого невозможно прочитать angularVelocity, приводит к ошибке
     */
    @Test
    fun `3) exception if angularVelocity fail`() {
        // Arrange
        val startDirection = 0
        val directionNumber = 360

        val stubRotable = object : IRotable {
            override var direction: Int = startDirection
            override val angularVelocity: Int
                get() = throw Throwable()
            override val directionNumber: Int = directionNumber
        }

        val rotateCommand = RotateCommand(stubRotable)

        // Act
        // Assert
        assertFails {
            rotateCommand.execute()
        }
    }

    /**
     * Попытка повернуть объект, у которого невозможно прочитать directionNumber, приводит к ошибке
     */
    @Test
    fun `4) exception if directionNumber fail`() {
        // Arrange
        val startDirection = 0
        val angularVelocity = 45

        val stubRotable = object : IRotable {
            override var direction: Int = startDirection
            override val angularVelocity: Int = angularVelocity
            override val directionNumber: Int
                get() = throw Throwable()
        }

        val rotateCommand = RotateCommand(stubRotable)

        // Act
        // Assert
        assertFails {
            rotateCommand.execute()
        }
    }

    /**
     * Попытка повернуть объект, у которого невозможно изменить угол поворота, приводит к ошибке
     */
    @Test
    fun `5) exception if rotate execute fail`() {
        // Arrange
        val startDirection = 0
        val angularVelocity = 45
        val directionNumber = 360

        val stubRotable = object : IRotable {
            override var direction: Int
                get() = startDirection
                set(value) {
                    throw Throwable()
                }
            override val angularVelocity: Int = angularVelocity
            override val directionNumber: Int = directionNumber
        }

        val rotateCommand = RotateCommand(stubRotable)

        // Act
        // Assert
        assertFails {
            rotateCommand.execute()
        }
    }
}