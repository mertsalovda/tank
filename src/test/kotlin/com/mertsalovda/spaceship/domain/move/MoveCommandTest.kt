package com.mertsalovda.spaceship.domain.move

import com.mertsalovda.spaceship.domain.command.move.MoveCommand
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.Vector
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import kotlin.test.assertFails

internal class MoveCommandTest {

    /**
     * Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3)
     * движение меняет положение объекта на (5, 8)
     */
    @Test
    fun `1) move from (12, 5) to (5, 8) if velocity (-7, 3)`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)
        val expectedPosition = Vector(5, 8)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val moveCommand = MoveCommand(stubMovable)
        // Act
        moveCommand.execute()
        val actualPosition = stubMovable.position

        // Assert
        assertEquals(expectedPosition, actualPosition)
    }

    /**
     * Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке
     */
    @Test
    fun `2) exception if position fail`() {
        // Arrange
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector
                get() = throw Throwable()
                set(value) {}
            override val velocity: Vector = velocity
        }

        val moveCommand = MoveCommand(stubMovable)

        // Act
        // Assert
        assertFails {
            moveCommand.execute()
        }
    }

    /**
     * Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке
     */
    @Test
    fun `3) exception if velocity fail`() {
        // Arrange
        val startPosition = Vector(12, 5)

        val stubMovable = object : IMovable {
            override var position: Vector
                get() = startPosition
                set(value) {}
            override val velocity: Vector
                get() = throw Throwable()
        }

        val moveCommand = MoveCommand(stubMovable)

        // Act
        // Assert
        assertFails {
            moveCommand.execute()
        }
    }

    /**
     * Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке
     */
    @Test
    fun `4) exception if move execute fail`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector
                get() = startPosition
                set(value) {
                    throw Throwable()
                }
            override val velocity: Vector
                get() = velocity
        }

        val moveCommand = MoveCommand(stubMovable)
        // Act
        // Assert
        assertFails {
            moveCommand.execute()
        }
    }
}