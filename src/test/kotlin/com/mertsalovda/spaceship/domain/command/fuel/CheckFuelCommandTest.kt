package com.mertsalovda.spaceship.domain.command.fuel

import com.mertsalovda.spaceship.domain.common.IFuelable
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.Vector
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertFails

internal class CheckFuelCommandTest {

    /**
     * Топлива достаточно на перемещение
     */
    @Test
    fun `1) check fuel success`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val fuelReserve = 100
        val burnFuelOnPath = 1

        val stubFuelable = object : IFuelable {
            override var fuel: Int
                get() = fuelReserve
                set(value) {}
            override val burnFuelOnPath: Int
                get() = burnFuelOnPath
        }

        val checkFuelCommand = CheckFuelCommand(stubMovable, stubFuelable)

        var isSuccess = true

        // Act
        try {
           checkFuelCommand.execute()
        } catch (ex: Exception) {
            isSuccess = false
        }

        // Assert
        assertTrue(isSuccess)
    }

    /**
     * Топлива не достаточно для перемещения
     */
    @Test
    fun `1) check fuel fail`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val fuelReserve = 1
        val burnFuelOnPath = 1

        val stubFuelable = object : IFuelable {
            override var fuel: Int
                get() = fuelReserve
                set(value) {}
            override val burnFuelOnPath: Int
                get() = burnFuelOnPath
        }

        val checkFuelCommand = CheckFuelCommand(stubMovable, stubFuelable)

        // Act
        // Assert
        assertFails {
            checkFuelCommand.execute()
        }
    }

}