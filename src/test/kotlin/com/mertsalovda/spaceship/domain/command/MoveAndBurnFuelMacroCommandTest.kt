package com.mertsalovda.spaceship.domain.command

import com.mertsalovda.spaceship.domain.command.fuel.BurnFuelCommand
import com.mertsalovda.spaceship.domain.command.fuel.CheckFuelCommand
import com.mertsalovda.spaceship.domain.command.move.MoveCommand
import com.mertsalovda.spaceship.domain.common.IFuelable
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.Vector
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertFails

internal class MoveAndBurnFuelMacroCommandTest {

    /**
     * Проверка выполнения макро команды движеия и сжигания топлива
     */
    @Test
    fun `1) check move and burn fuel`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val fuelReserve = 100
        val burnFuelOnPath = 1

        val stubFuelable = object : IFuelable {
            override var fuel: Int = fuelReserve
            override val burnFuelOnPath: Int = burnFuelOnPath
        }

        val checkFuelCommand = CheckFuelCommand(stubMovable, stubFuelable)
        val moveCommand = MoveCommand(stubMovable)
        val burnFuelCommand = BurnFuelCommand(stubMovable, stubFuelable)

        val macroCommand = MacroCommand(listOf(checkFuelCommand, moveCommand, burnFuelCommand))

        val expectedValues = mapOf<String, Any>(
            "fuel" to 93,
            "newPosition" to Vector(5, 8)
        )
        // Act
        macroCommand.execute()
        val actualValues = mapOf<String, Any>(
            "fuel" to stubFuelable.fuel,
            "newPosition" to stubMovable.position
        )

        // Assert
        val valid = expectedValues == actualValues
        assertTrue(valid, "expected $expectedValues not equals actual $actualValues")
    }

    /**
     * Выброс исключения проверки топлива в макрокоманде.
     * В запасе 1 единица топлива, а для перемещения необходимо 7 единиц топлива.
     */
    @Test
    fun `2) check fuel is fail in macro command`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val fuelReserve = 1
        val burnFuelOnPath = 1

        val stubFuelable = object : IFuelable {
            override var fuel: Int = fuelReserve
            override val burnFuelOnPath: Int = burnFuelOnPath
        }

        val checkFuelCommand = CheckFuelCommand(stubMovable, stubFuelable)
        val moveCommand = MoveCommand(stubMovable)
        val burnFuelCommand = BurnFuelCommand(stubMovable, stubFuelable)

        val macroCommand = MacroCommand(listOf(checkFuelCommand, moveCommand, burnFuelCommand))
        // Act
        // Assert
        assertFails {
            macroCommand.execute()
        }
    }
}