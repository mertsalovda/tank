package com.mertsalovda.spaceship.domain.command.fuel

import com.mertsalovda.spaceship.domain.common.IFuelable
import com.mertsalovda.spaceship.domain.common.IMovable
import com.mertsalovda.spaceship.domain.common.Vector
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class BurnFuelCommandTest {

    /**
     * Изменилось количество топлива при перемещении
     */
    @Test
    fun `1) change fuel value`() {
        // Arrange
        val startPosition = Vector(12, 5)
        val velocity = Vector(-7, 3)

        val stubMovable = object : IMovable {
            override var position: Vector = startPosition
            override val velocity: Vector = velocity
        }

        val fuelReserve = 100
        val burnFuelOnPath = 1

        val stubFuelable = object : IFuelable {
            override var fuel: Int = fuelReserve
            override val burnFuelOnPath: Int = burnFuelOnPath
        }

        val burnFuelCommand = BurnFuelCommand(stubMovable, stubFuelable)

        val expectedFuel = 93

        // Act
        burnFuelCommand.execute()
        val actualFuel = stubFuelable.fuel

        // Assert
        assertEquals(expectedFuel, actualFuel)
    }
}