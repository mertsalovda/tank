package com.mertsalovda.spaceship.domain.exception

import com.mertsalovda.spaceship.domain.common.ICommand
import com.mertsalovda.spaceship.domain.exception.command.Log
import com.mertsalovda.spaceship.domain.exception.command.OneRepeatCommand
import com.mertsalovda.spaceship.domain.exception.command.TwoRepeatCommand
import org.junit.jupiter.api.Test
import java.util.*

internal class ExceptionHandlerTest {

    /**
     * Запись исключения в лог
     */
    @Test
    fun `1) write to log`() {
        // Arrange
        val commandQueue: Queue<ICommand> = LinkedList()
        val commandStub = SubCommand()
        commandQueue.add(commandStub)

        val exceptionHandler = ExceptionHandler()

        exceptionHandler.setup(
            commandType = SubCommand::class,
            exceptionType = Exception::class,
            action = { command, exception ->
                commandQueue.add(Log(command, exception))
            }
        )
        // Act
        val comm = commandQueue.poll()
        try {
            comm.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(comm, exception)
        }

        // Assert
        assert(commandQueue.poll() is Log)
    }




    class SubCommand : ICommand {
        override fun execute() = throw Exception()
    }


    /**
     * Повторить команду, выбросившую исключение
     */
    @Test
    fun `2) repeat command`() {
        // Arrange
        val commandQueue: Queue<ICommand> = LinkedList()
        val commandStub = SubCommand()
        commandQueue.add(commandStub)

        val exceptionHandler = ExceptionHandler()

        exceptionHandler.setup(
            commandType = SubCommand::class,
            exceptionType = Exception::class,
            action = { command, _ ->
                commandQueue.add(OneRepeatCommand(command))
            }
        )

        exceptionHandler.setup(
            commandType = OneRepeatCommand::class,
            exceptionType = Exception::class,
            action = { command, exception ->
                commandQueue.add(Log(command, exception))
            }
        )
        // Act
        val comm = commandQueue.poll()
        try {
            comm.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(comm, exception)
        }

        val repeatCommand = commandQueue.poll()
        try {
            repeatCommand.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(repeatCommand, exception)
        }

        // Assert
        assert(commandQueue.poll() is Log)
    }


    /**
     * Дважды повторить команду, выбросившую исключение
     */
    @Test
    fun `3) twice repeat command`() {
        // Arrange
        val commandQueue: Queue<ICommand> = LinkedList()
        val commandStub = SubCommand()
        commandQueue.add(commandStub)

        val exceptionHandler = ExceptionHandler()

        exceptionHandler.setup(
            commandType = SubCommand::class,
            exceptionType = Exception::class,
            action = { command, _ ->
                commandQueue.add(OneRepeatCommand(command))
            }
        )

        exceptionHandler.setup(
            commandType = OneRepeatCommand::class,
            exceptionType = Exception::class,
            action = { command, _ ->
                commandQueue.add(TwoRepeatCommand(command))
            }
        )

        exceptionHandler.setup(
            commandType = TwoRepeatCommand::class,
            exceptionType = Exception::class,
            action = { command, exception ->
                commandQueue.add(Log(command, exception))
            }
        )
        // Act
        val comm = commandQueue.poll()
        try {
            comm.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(comm, exception)
        }
        // Первое повторение
        val twoRepeatCommand = commandQueue.poll()
        try {
            twoRepeatCommand.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(twoRepeatCommand, exception)
        }

        // Второе повторение
        val oneRepeatCommand = commandQueue.poll()
        try {
            oneRepeatCommand.execute()
        } catch (exception: Exception) {
            exceptionHandler.handle(oneRepeatCommand, exception)
        }

        // Assert
        assert(commandQueue.poll() is Log)
    }
}