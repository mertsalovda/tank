package com.mertsalovda.sort.factory

import com.mertsalovda.sort.BubbleSort
import com.mertsalovda.sort.MergeSort
import com.mertsalovda.sort.SelectionSort
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Test
import kotlin.test.assertIs
import kotlin.test.assertNull
import kotlin.test.assertTrue

internal class AbstractSorterFactoryTest {

    private lateinit var abstractFactory: AbstractSorterFactoryImpl
    private val selectionSort = "selection"
    private val bubbleSort = "bubble"
    private val mergeSort = "merge"

    @BeforeEach
    fun setUp() {
        abstractFactory = AbstractSorterFactoryImpl()
        initAbstractFactory(abstractFactory)
    }

    private fun initAbstractFactory(abstractSorterFactory: AbstractSorterFactoryImpl) {
        abstractSorterFactory.setup(selectionSort) {
            SelectionSortFactory()
        }
        abstractSorterFactory.setup(bubbleSort) {
            BubbleSortFactory()
        }
        abstractSorterFactory.setup(mergeSort) {
            MergeSortFactory()
        }
    }

    @Test
    fun `1) Тест на создание SelectionSortFactory`() {
        // Arrange
        // Act
        val actualFactory = abstractFactory.createSorter(selectionSort)
        // Assert
        assertIs<SelectionSortFactory>(actualFactory, "Фактический тип ${actualFactory?.javaClass?.name}, ожидаемый тип SelectionSortFactory")
    }

    @Test
    fun `2) Тест на создание BubbleSortFactory`() {
        // Arrange
        // Act
        val actualFactory = abstractFactory.createSorter(bubbleSort)
        // Assert
        assertIs<BubbleSortFactory>(actualFactory, "Фактический тип ${actualFactory?.javaClass?.name}, ожидаемый тип BubbleSortFactory")
    }

    @Test
    fun `3) Тест на создание MergeSortFactory`() {
        // Arrange
        // Act
        val actualFactory = abstractFactory.createSorter(mergeSort)
        // Assert
        assertIs<MergeSortFactory>(actualFactory, "Фактический тип ${actualFactory?.javaClass?.name}, ожидаемый тип MergeSortFactory")
    }

    @Test
    fun `4) Тест на создание SelectionSort`() {
        // Arrange
        val factory = abstractFactory.createSorter(selectionSort)
        // Act
        val actualSort = factory?.createSorter()
        // Assert
        assertIs<SelectionSort>(actualSort, "Фактический тип ${actualSort?.javaClass?.name}, ожидаемый тип SelectionSort")
    }

    @Test
    fun `5) Тест на создание BubbleSort`() {
        // Arrange
        val factory = abstractFactory.createSorter(bubbleSort)
        // Act
        val actualSort = factory?.createSorter()
        // Assert
        assertIs<BubbleSort>(actualSort, "Фактический тип ${actualSort?.javaClass?.name}, ожидаемый тип BubbleSort")
    }

    @Test
    fun `6) Тест на создание MergeSort`() {
        // Arrange
        val factory = abstractFactory.createSorter(mergeSort)
        // Act
        val actualSort = factory?.createSorter()
        // Assert
        assertIs<MergeSort>(actualSort, "Фактический тип ${actualSort?.javaClass?.name}, ожидаемый тип MergeSort")
    }

    @Test
    fun `7) Тест на создание SelectionSort, BubbleSort и MergeSort из одной абстрактной фабрики`() {
        // Arrange
        val selectionSortFactory = abstractFactory.createSorter(selectionSort)
        val bubbleSortFactory = abstractFactory.createSorter(bubbleSort)
        val mergeSortFactory = abstractFactory.createSorter(mergeSort)
        // Act
        val actualSelectionSort = selectionSortFactory?.createSorter() is SelectionSort
        val actualBubbleSort = bubbleSortFactory?.createSorter() is BubbleSort
        val actualMergeSort = mergeSortFactory?.createSorter() is MergeSort
        // Assert
        assertTrue(actualSelectionSort && actualBubbleSort && actualMergeSort, "SelectionSort: $actualSelectionSort, BubbleSort: $actualBubbleSort, MergeSort: $actualMergeSort")
    }

    @Test
    fun `8) Получение несуществующего объекта из фабрики`() {
        // Arrange
        // Act
        val actualFactory = abstractFactory.createSorter("don't exist")
        // Assert
        assertNull(actualFactory)
    }
}