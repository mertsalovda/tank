package com.mertsalovda.sort

import com.mertsalovda.sort.helpers.SortHelper
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

internal class SortHelperTest {

    private val nonSortedArrayFilePath = "src/test/resources/non_sorted_array_test"
    private val sortedArrayFilePath = "src/test/resources/sorted_array_test"

    private val nonSortedArrayText = "4 3 1 2 7 9 10 12 5 8 13 11 6"

    private val expectedSortedArray = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)

    private val selectionSort = "selection"
    private val bubbleSort = "bubble"
    private val mergeSort = "merge"

    @BeforeEach
    fun setUp() {
        val file = File(nonSortedArrayFilePath)
        file.writeText(nonSortedArrayText)
    }

    @AfterEach
    fun tearDown() {
        val nonSortedFile = File(sortedArrayFilePath)
        if (nonSortedFile.exists()) {
            nonSortedFile.delete()
        }
        val sortedFile = File(nonSortedArrayFilePath)
        if (sortedFile.exists()) {
            sortedFile.delete()
        }
    }

    //@Test
    fun `9) Сортировка выбором`() {
        // Arrange
        val sortHelper = SortHelper(selectionSort, nonSortedArrayFilePath, sortedArrayFilePath)
        // Act
        sortHelper.sort()
        val actual = mutableListOf<Int>()
        File(sortedArrayFilePath).readLines()
            .getOrNull(1)
            ?.split(" ")
            ?.forEach {
                actual.add(it.toInt())
            }
        // Assert
        assertArrayEquals(expectedSortedArray, actual.toTypedArray())
    }

    //@Test
    fun `10) Пузырьковая сортировка`() {
        // Arrange
        val sortHelper = SortHelper(bubbleSort, nonSortedArrayFilePath, sortedArrayFilePath)
        // Act
        sortHelper.sort()
        val actual = mutableListOf<Int>()
        File(sortedArrayFilePath).readLines()
            .getOrNull(1)
            ?.split(" ")
            ?.forEach {
                actual.add(it.toInt())
            }
        // Assert
        assertArrayEquals(expectedSortedArray, actual.toTypedArray())
    }

    //@Test
    fun `11) Сортировка слиянием`() {
        // Arrange
        val sortHelper = SortHelper(mergeSort, nonSortedArrayFilePath, sortedArrayFilePath)
        // Act
        sortHelper.sort()
        val actual = mutableListOf<Int>()
        File(sortedArrayFilePath).readLines()
            .getOrNull(1)
            ?.split(" ")
            ?.forEach {
                actual.add(it.toInt())
            }
        // Assert
        assertArrayEquals(expectedSortedArray, actual.toTypedArray())
    }
}